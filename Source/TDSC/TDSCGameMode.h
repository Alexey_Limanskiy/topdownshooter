// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSCGameMode.generated.h"

UCLASS(minimalapi)
class ATDSCGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDSCGameMode();
};



