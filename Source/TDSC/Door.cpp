// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = Root;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	Mesh->SetupAttachment(Root);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision"));
	BoxCollision->SetupAttachment(Root);

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ADoor::BeginOverlap);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &ADoor::EndOverlap);

	bCanBeOpenedClosed = false;
	OpeningSpeed = 3.0f;
	DoorOffset = -140.0f;
	CanPlaying = true;
	StartPlaySound = 0.3f;

	static ConstructorHelpers::FObjectFinder<USoundCue> DoorSoundCueObject_01(TEXT("SoundCue'/Game/FX/Sound/Door01_Cue.Door01_Cue'"));
	if (DoorSoundCueObject_01.Succeeded())
	{
		Door01 = DoorSoundCueObject_01.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DoorSoundCueObject_02(TEXT("SoundCue'/Game/FX/Sound/Door02_Cue.Door02_Cue'"));
	if (DoorSoundCueObject_02.Succeeded())
	{
		Door02= DoorSoundCueObject_02.Object;
	}
	

}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();

	
	
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	/*if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(0, 0.1f, FColor::Red, TEXT("Tick"));
	}*/


	ChangeDoorState();


	if (bCanBeOpenedClosed)
	{
		if (bIsOpen)
		{
			DoorClosed();
		}
		else
		{
			DoorOpened();
		}

	}
	
}

void ADoor::ChangeDoorState()
{
	
	if (Mesh->GetRelativeLocation().Y <= DoorOffset)
	{
		bIsOpen = true;
		
	}

	if (Mesh->GetRelativeLocation().Y >= 0.0f)
	{
		bIsOpen = false;
		
	}
	
}

void ADoor::DoorOpened()
{
	

	if (Mesh != nullptr)
	{
		float CurrentDoorLocation_Y;

		CurrentDoorLocation_Y = Mesh->GetRelativeLocation().Y;

		CurrentDoorLocation_Y -= OpeningSpeed;

		Mesh->SetRelativeLocation(FVector(0.0f, CurrentDoorLocation_Y, 0.0f));

		DoorSoundPlaying(Door01);

		if (CurrentDoorLocation_Y <= DoorOffset)
		{
			bCanBeOpenedClosed = false;
			CanPlaying = true;
			DoorSoundPlaying(Door02);
			SetActorTickEnabled(false);
			CanPlaying = true;
		}
	}


}

void ADoor::DoorClosed()
{
	if (Mesh != nullptr)
	{
		float CurrentDoorLocation_Y;

		CurrentDoorLocation_Y = Mesh->GetRelativeLocation().Y;

		CurrentDoorLocation_Y += OpeningSpeed;

		Mesh->SetRelativeLocation(FVector(0.0f, CurrentDoorLocation_Y, 0.0f));

		DoorSoundPlaying(Door01);
		
		if (CurrentDoorLocation_Y >= 0.0f)
		{
			bCanBeOpenedClosed = false;
			CanPlaying = true;
			DoorSoundPlaying(Door02);
			SetActorTickEnabled(false);
			CanPlaying = true;
			
		}
	}




}

void ADoor::BeginOverlap(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if ((OtherActor != nullptr) && (OtherActor != nullptr) && (OtherComp != nullptr))
	{
		bCanBeOpenedClosed = true;
		SetActorTickEnabled(true);
		
	}

}

void ADoor::EndOverlap(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if ((OtherActor != nullptr) && (OtherActor != nullptr) && (OtherComp != nullptr))
	{
				
		bCanBeOpenedClosed = true;
		SetActorTickEnabled(true);
	}

}

void ADoor::DoorSoundPlaying(USoundCue* NewSound)
{
	if ((CanPlaying) && (NewSound !=nullptr))
	{
		UGameplayStatics::PlaySoundAtLocation(this, NewSound, Mesh->GetRelativeLocation(), 1.0f, 1.0f, StartPlaySound);
		CanPlaying = false;


	}
}





