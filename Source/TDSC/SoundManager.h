// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSCCharacter.h"
#include "Sound/SoundCue.h"
#include "SoundManager.generated.h"


UCLASS()
class TDSC_API ASoundManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASoundManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
		class USoundCue* CosmoSound;

	UPROPERTY()
		FTimerHandle SoundTimer;
	UPROPERTY()
		float CurrentSoundLength;
	UPROPERTY()
		FVector SoundLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ATDSCCharacter* Character;

	UFUNCTION()
		void PlayCosmoSound();

};
