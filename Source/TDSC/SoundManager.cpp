// Fill out your copyright notice in the Description page of Project Settings.


#include "SoundManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASoundManager::ASoundManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	static ConstructorHelpers::FObjectFinder<USoundCue> CosmoSoundObject(TEXT("SoundCue'/Game/FX/Sound/CosmoSound_Cue.CosmoSound_Cue'"));
	if (CosmoSoundObject.Succeeded())
	{
		CosmoSound = CosmoSoundObject.Object;
	}

	CurrentSoundLength = 202.14f;

}

// Called when the game starts or when spawned
void ASoundManager::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(SoundTimer, this, &ASoundManager::PlayCosmoSound, CurrentSoundLength, true, 0.0f);

	Character = Cast <ATDSCCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
	
	PlayCosmoSound();

}

// Called every frame
void ASoundManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (Character)
	{
		SoundLocation = Character->GetActorLocation();
	}
	

}

void ASoundManager::PlayCosmoSound()
{
	if (CosmoSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, CosmoSound, SoundLocation, 1.0f, 1.0f, 0.0f);
		
	}
	
}

